import 'package:flutter/material.dart';
import 'package:oceanus_flutter/data_service.dart';
import 'package:oceanus_flutter/detections.dart';
import 'package:oceanus_flutter/video_feed.dart';
import 'package:oceanus_flutter/statistics.dart';

void main() => runApp(Oceanus());

class Oceanus extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Oceanus',
      home: Center(child: PageLayout()),
    );
  }
}

class PageLayout extends StatefulWidget {
  PageLayout({Key key}) : super(key: key);

  @override
  _PageLayout createState() => _PageLayout();
}

class _PageLayout extends State<PageLayout> {
  int _selectedIndex = 0;
  // This variable points to their respective pages based on the current selection of the navigation bar.
  static List<Widget> _widgetOptions = <Widget>[
    Detections(),
    VideoFeed(),
    Statistics(),
  ];

  /// Updates the currently selected item in the navigation bar.
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  /// Initializes the data service (fetches the IP)
  void initDataService() {
    DataService dataService = new DataService();
    dataService.initState();
  }

  @override
  Widget build(BuildContext context) {
    initDataService();

    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.photo_library),
            title: Text('Detections'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.videocam),
            title: Text('Video Feed'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.data_usage),
            title: Text('Statistics'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue[300],
        onTap: _onItemTapped,
      ),
    );
  }
}
