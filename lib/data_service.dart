import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:gateway/gateway.dart';

String ip;
String apiUrl;
Future<String> _apiUrl;
String streamingUrl;

Future<String> getGatewayIP() async {
  _apiUrl = _apiUrl ?? _getGatewayIP();
  return _apiUrl;
}

/// Gets the current wifi network gateway IP(when using the apparatus hotspot we get its ip)
Future<String> _getGatewayIP() async {
  Gateway gt = await Gateway.info;
  return gt.ip;
}

class DataService {
  void initState() async {
    // Initializes the apiUrl and streamingUrl variable with the current gateway IP
    //ip = "http://192.168.6.99"; // ARDITI PC
    //ip = await getGatewayIP();
    //ip = "http://$apiUrl"; // Gateway IP
    ip = "http://192.168.1.47"; // HOME
    //ip = "http://192.168.6.141"; // ARDITI
    //ip = "http://10.2.0.170"; // M-ITI

    apiUrl = ip + ":5000";
    streamingUrl = ip + ":5001";
  }
}

/// Converts JSON to a list of objects by grabbing the "data" item from json and iterating on it.
dynamic iterateJson(String listType, List<dynamic> returnList, String jsonStr) {
  Map<String, dynamic> myMap = json.decode(jsonStr);
  List<dynamic> data = myMap["data"];

  switch (listType) {
    case "Detection":
      {
        data.forEach((item) {
          returnList.add(UnreviewedDetection.fromJson(item));
        });
      }
      break;
    case "Object":
      {
        data.forEach((item) {
          returnList.add(Object.fromJson(item));
        });
      }
      break;
    case "ClassStats":
      {
        data.forEach((item) {
          returnList.add(ClassStats.fromJson(item));
        });
      }
      break;
  }
  return returnList;
}

class UnreviewedDetection {
  final int id;
  final String imagePath;
  final int imageWidth;
  final int imageHeight;
  final double lat;
  final double lon;
  final int userConfirmation;
  final String comment;
  final String createdAt;

  UnreviewedDetection(
      {this.id,
      this.imagePath,
      this.imageWidth,
      this.imageHeight,
      this.lat,
      this.lon,
      this.userConfirmation,
      this.comment,
      this.createdAt});

  factory UnreviewedDetection.fromJson(Map<String, dynamic> json) {
    return UnreviewedDetection(
      id: json['id'],
      imagePath: json['image_path'],
      imageWidth: json['image_width'],
      imageHeight: json['image_height'],
      lat: json['latitude'],
      lon: json['longitude'],
      userConfirmation: json['user_confirmation'],
      comment: json['comment'],
      createdAt: json['created_at'],
    );
  }
}

/// Fetches the unreviewed detections present on the Oceanus apparatus.
Future<List<UnreviewedDetection>> fetchDetections() async {
  try {
    await getGatewayIP();
    final response = await http.get(apiUrl + "/detection/unreviewed");

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List<UnreviewedDetection> returnList = List<UnreviewedDetection>();
      returnList = iterateJson("Detection", returnList, response.body);
      return returnList;
      //return UnreviewedDetection.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      //TODO: Show message on screen instead of throwing exceptions.
      throw Exception('Failed to load unreviewed detections.');
    }
  } catch (err) {
    print("HTTP ERROR: " + err.toString());
    //TODO: Show message on screen instead of throwing exceptions.
    throw Exception(
        "Failed to connect to Oceanus.\nMake sure you are connected to the Oceanus' Hotspot.");
  }
}

/// Updates the selected detection with the user feedback.
Future<http.Response> updateDetectionFeedback(
    int detectionID, int userConfirmation, String comment) async {
  return await http.put(
    '$apiUrl/detection',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'detection_id': detectionID.toString(),
      'user_confirmation': userConfirmation.toString(),
      'comment': comment,
    }),
  );
}

class Object {
  final int id;
  final int detectionID;
  final int classID;
  final String className;
  final double accuracy;
  final String createdAt;

  Object(
      {this.id,
      this.detectionID,
      this.classID,
      this.className,
      this.accuracy,
      this.createdAt});

  factory Object.fromJson(Map<String, dynamic> json) {
    return Object(
      id: json['id'],
      detectionID: json['detection_id'],
      classID: json['class_id'],
      className: json['class_name'],
      accuracy: json['accuracy'],
      createdAt: json['time'],
    );
  }
}

/// Fetches the unreviewed detections present on the Oceanus apparatus.
Future<List<Object>> fetchObjectsOfDetection(int detectionID) async {
  try {
    await getGatewayIP();
    final response = await http.get(apiUrl + "/object/detection/$detectionID");

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      List<Object> returnList = List<Object>();
      returnList = iterateJson("Object", returnList, response.body);
      return returnList;
      //return UnreviewedDetection.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      //TODO: Show message on screen instead of throwing exceptions.
      throw Exception('Failed to load objects.');
    }
  } catch (_) {
    //TODO: Show message on screen instead of throwing exceptions.
    throw Exception(
        "Failed to connect to Oceanus.\nMake sure you are connected to the Oceanus' Hotspot.");
  }
}

class ClassStats {
  final String className;
  final double accuracy;
  final int numberOfDetections;

  ClassStats({this.className, this.accuracy, this.numberOfDetections});

  factory ClassStats.fromJson(Map<String, dynamic> json) {
    return ClassStats(
      className: json['name'],
      accuracy: json['accuracy'],
      numberOfDetections: json['detections'],
    );
  }
}

/// Fetches the unreviewed detections present on the Oceanus apparatus. If pastHours is 0 will fetch  All Time.
Future<List<ClassStats>> fetchAllClassStats(int pastHours) async {
  // List<ClassStats> returnList = List<ClassStats>();

  // Map<String, dynamic> jsonItem = {
  //   "name": "turtle",
  //   "accuracy": 0.92,
  //   "numberOfDetections": 282
  // };
  // ClassStats firstClass = ClassStats.fromJson(jsonItem);
  // returnList.add(firstClass);
  // returnList.add(firstClass);
  // return returnList;
  try {
    await getGatewayIP();
    final response = await http
        .get(apiUrl + "/statistics/numberofDetectionsAndAverage/$pastHours");

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.

      Map<String, dynamic> jsonMap = json.decode(response.body);
      List<ClassStats> returnList = List<
          ClassStats>(); // TODO: always return data and a message, if data is empty display the message in receiving page

      if (jsonMap['success']) {
        returnList = iterateJson("ClassStats", returnList, response.body);
      } else {
        print("There is no data for that time frame.");
        //throw Exception("There is no data for that time frame.");
      }
      return returnList;
      //return UnreviewedDetection.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      //TODO: Show message on screen instead of throwing exceptions.
      throw Exception(
          "Failed to connect to Oceanus.\nMake sure you are connected to the Oceanus' Hotspot.");
    }
  } catch (e) {
    //TODO: Show message on screen instead of throwing exceptions.
    throw Exception(e);
  }
}
