dynamic average(List<dynamic> values) {
  dynamic sum = 0;
  values.forEach((element) {
    sum += element;
  });
  return sum / values.length;
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
