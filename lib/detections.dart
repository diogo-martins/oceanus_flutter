import 'dart:async';

import 'package:flutter/material.dart';

import 'package:oceanus_flutter/data_service.dart';
import 'package:oceanus_flutter/detection_feedback.dart';

class Detections extends StatefulWidget {
  @override
  _Detections createState() => _Detections();
}

class _Detections extends State<Detections> {
  Color color = Colors.white;
  FutureBuilder<List<UnreviewedDetection>> futureUnreviewedDetectionBuilder;

  @override
  void initState() {
    super.initState();
    futureUnreviewedDetectionBuilder = createFutureBuilder();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<List<UnreviewedDetection>> futureDetectionsList() async {
    return fetchDetections();
  }

  Future forceRefreshDetections() async {
    await futureDetectionsList();
    setState(() {
      futureUnreviewedDetectionBuilder = createFutureBuilder();
    });
  }

  FutureBuilder createFutureBuilder() {
    return FutureBuilder<List<UnreviewedDetection>>(
      future: futureDetectionsList(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.length == 0) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.hourglass_empty,
                  color: Colors.grey,
                  size: 102.0,
                ),
                Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 5.0, horizontal: 30.0),
                    child: Text(
                      "There are no unreviewd detections at the moment.",
                      textAlign: TextAlign.center,
                      style: DefaultTextStyle.of(context)
                          .style
                          .apply(fontSizeFactor: 1.5),
                    )),
              ],
            );
          } else {
            return GridView.builder(
              physics: BouncingScrollPhysics(),
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                final item = snapshot.data[index];
                final detectionID = item.id;

                // Individual image is displayed here
                return GestureDetector(
                    onTap: () {
                      // Navigates to detection feedback providing the selected detection data
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetectionFeedback(
                                      item, forceRefreshDetections)))
                          .then((value) {});
                    },
                    child: Card(
                      //color: Color.fromRGBO(225, 225, 225, 100),
                      color: color,
                      child: Image.network(
                        "$apiUrl/detection/$detectionID/thumbnail",
                        fit: BoxFit.cover,
                        loadingBuilder: (context, child, progress) {
                          return progress == null
                              ? child
                              : Center(child: CircularProgressIndicator());
                        },
                      ),
                    ));
              },
            );
          }
        } else if (snapshot.hasError) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.cloud_off,
                color: Colors.grey,
                size: 102.0,
              ),
              Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 5.0, horizontal: 30.0),
                  child: Text(
                    "${snapshot.error}".replaceAll("Exception: ", ""),
                    textAlign: TextAlign.center,
                    style: DefaultTextStyle.of(context)
                        .style
                        .apply(fontSizeFactor: 1.5),
                  )),
            ],
          );
        }

        // By default, show a loading spinner and warning text.
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 0.0),
            ),
            Text(
              "Fetching Detections.",
              style:
                  DefaultTextStyle.of(context).style.apply(fontSizeFactor: 1.5),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
            ),
            // Text("Make sure you are connected to the Oceanus' Hotspot."), // TODO: Check if this is related to the connection or if this message is only needed in data_service.dart
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: const Text('Detections'),
        ),
        //body: Center(child: futureUnreviewedDetectionBuilder),
        body: Center(child: createFutureBuilder())
        //     StreamBuilder(
        //   stream: _streamController.stream,
        //   builder: (BuildContext context, AsyncSnapshot snapshot) {
        //     if (snapshot.hasData)
        //       return ListView(
        //         children: snapshot.data.map((document) {
        //           return ListTile(
        //             title: Text(document['title']),
        //             subtitle: Text(document['type']),
        //           );
        //         }).toList(),
        //       );
        //     return Text('Loading...');
        //   },
        // ),
        );
  }
}
