import 'package:flutter/material.dart';

import 'package:oceanus_flutter/data_service.dart';
import 'package:webview_flutter/webview_flutter.dart';

class VideoFeed extends StatefulWidget {
  @override
  _VideoFeed createState() => _VideoFeed();
}

class _VideoFeed extends State<VideoFeed> {
  // final Completer<WebViewController> _controller =
  //     Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Video Feed'),
      ),
      body: Center(
        child: WebView(
          initialUrl: "$streamingUrl",
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            //_controller.complete(webViewController);
          },
        ),
      ),
    );
  }
}
