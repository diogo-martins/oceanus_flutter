import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

import 'package:oceanus_flutter/data_service.dart';

import 'dart:math';

double roundDouble(double value, int places) {
  double mod = pow(10.0, places);
  return ((value * mod).round().toDouble() / mod);
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

class Statistics extends StatefulWidget {
  @override
  _Statistics createState() => _Statistics();
}

class _Statistics extends State<Statistics> {
  int dropdownValue = 0;

  Future<List<ClassStats>> futureClassStatsList(int pastHours) async {
    return fetchAllClassStats(pastHours);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Statistics'),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            DropdownButton<int>(
              value: dropdownValue,
              items: [
                DropdownMenuItem(
                  child: Text("Last Hour"),
                  value: 1,
                ),
                DropdownMenuItem(
                  child: Text("Last Two Hours"),
                  value: 2,
                ),
                DropdownMenuItem(child: Text("Today"), value: 24),
                DropdownMenuItem(child: Text("All Time"), value: 0)
              ],
              onChanged: (int newValue) {
                setState(() {
                  dropdownValue = newValue;
                });
              },
            ),
            FutureBuilder<List<ClassStats>>(
              future: futureClassStatsList(dropdownValue),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length == 0) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.hourglass_empty,
                          color: Colors.grey,
                          size: 102.0,
                        ),
                        Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: 5.0, horizontal: 30.0),
                            child: Text(
                              "There are no detections recorded for this time frame.",
                              textAlign: TextAlign.center,
                              style: DefaultTextStyle.of(context)
                                  .style
                                  .apply(fontSizeFactor: 1.5),
                            )),
                      ],
                    );
                  } else {
                    return new ListView.builder(
                        itemCount: snapshot.data.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(children: [
                            ListTile(
                              //leading: Icon(Icons.arrow_forward_ios),
                              title: Text(
                                  snapshot.data[index].className.capitalize() +
                                      " (" +
                                      snapshot.data[index].numberOfDetections
                                          .toString() +
                                      ")"),
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(roundDouble(
                                              100 *
                                                  snapshot.data[index].accuracy,
                                              2)
                                          .toString() +
                                      "%"),
                                ],
                              ),
                            ),
                            LinearPercentIndicator(
                              percent: snapshot.data[index].accuracy,
                              progressColor: Colors.blue,
                              lineHeight: 15,
                            )
                          ]);
                        });
                  }
                } else if (snapshot.hasError) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.cloud_off,
                        color: Colors.grey,
                        size: 102.0,
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 30.0),
                          child: Text(
                            "${snapshot.error}".replaceAll("Exception: ", ""),
                            textAlign: TextAlign.center,
                            style: DefaultTextStyle.of(context)
                                .style
                                .apply(fontSizeFactor: 1.5),
                          )),
                    ],
                  );
                }

                // By default, show a loading spinner and warning text.
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 15.0, horizontal: 0.0),
                    ),
                    Text(
                      "Fetching statistics.",
                      style: DefaultTextStyle.of(context)
                          .style
                          .apply(fontSizeFactor: 1.5),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
                    ),
                    // Text("Make sure you are connected to the Oceanus' Hotspot."), // TODO: Check if this is related to the connection or if this message is only needed in data_service.dart
                  ],
                );
              },
            )
          ]),
    );
  }
}
