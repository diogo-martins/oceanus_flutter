import 'dart:io';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import 'package:oceanus_flutter/data_service.dart';
import 'package:oceanus_flutter/functions.dart';

class DetectionFeedback extends StatefulWidget {
  final UnreviewedDetection selectedDetection;
  final Function callbackFunction;

  DetectionFeedback(this.selectedDetection, this.callbackFunction);

  @override
  _DetectionFeedback createState() => _DetectionFeedback();
}

class _DetectionFeedback extends State<DetectionFeedback> {
  int detectionID;
  Future<List<Object>> furuteObjectsOfDetection;
  final commentTextFieldController =
      TextEditingController(); // Used to get the textfield value.

  @override
  void dispose() {
    commentTextFieldController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    detectionID = widget.selectedDetection.id;
    furuteObjectsOfDetection = fetchObjectsOfDetection(detectionID);
    super.initState();
  }

  fetchObjectsData() {
    return FutureBuilder<List<Object>>(
        future: furuteObjectsOfDetection,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            // List containing the accuracy of every object in the current detection
            List<double> objectsAccuracy = new List<double>();
            // List of the objects and their quantity  in the current detection
            Map<String, int> objectsQuantity = new Map<String, int>();

            // Iterate the data fetched and fill the previous lists
            snapshot.data.forEach((element) {
              objectsAccuracy.add(element.accuracy);

              if (objectsQuantity.containsKey(element.className)) {
                objectsQuantity.update(
                    element.className, (value) => value = value + 1);
              } else {
                objectsQuantity[element.className] = 1;
              }
            });

            double averageAccuracy =
                num.parse((100 * average(objectsAccuracy)).toStringAsFixed(2));

            // Generate the list of widgets to display all objects and quantity
            List<Widget> objects = new List<Widget>();
            objectsQuantity.forEach((key, value) {
              objects.add(Text(key.capitalize() + " ($value)",
                  style: DefaultTextStyle.of(context)
                      .style
                      .apply(fontSizeFactor: 1.5)));
            });

            return (Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "$averageAccuracy%",
                        style: DefaultTextStyle.of(context)
                            .style
                            .apply(fontSizeFactor: 3),
                      ),
                      ListView(
                        shrinkWrap: true,
                        children: objects,
                      )
                    ])));
          } else if (snapshot.hasError) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.cloud_off,
                  color: Colors.grey,
                  size: 64.0,
                ),
                Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 5.0, horizontal: 30.0),
                    child: Text(
                      "${snapshot.error}".replaceAll("Exception: ", ""),
                      textAlign: TextAlign.center,
                      style: DefaultTextStyle.of(context)
                          .style
                          .apply(fontSizeFactor: 1.5),
                    ))
              ],
            );
          } // By default, show a loading spinner.
          return Padding(
              padding: EdgeInsets.all(15),
              child: Center(child: CircularProgressIndicator()));
        });
  }

  @override
  Widget build(BuildContext context) {
    var appBar = AppBar(
      title: const Text('Detection Feedback'),
    );

    return Scaffold(
      appBar: appBar,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                fetchObjectsData(),
                Container(
                  height: (MediaQuery.of(context).size.height -
                          appBar.preferredSize.height) /
                      2.5,
                  child: ClipRect(
                    child: PhotoView(
                        //TODO: Implement image carousel to display the classified and raw images.
                        imageProvider:
                            NetworkImage("$apiUrl/detection/$detectionID/1"),
                        maxScale: PhotoViewComputedScale.covered * 3.0,
                        minScale: PhotoViewComputedScale.contained * 1.0,
                        initialScale: PhotoViewComputedScale.covered,
                        backgroundDecoration: BoxDecoration(
                          color: Color(0xffe0e0e0),
                        )),
                  ),
                ),
                Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                    child: Center(
                        child: Text(
                      "Is this classification absolutely correct?",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 19, decoration: TextDecoration.none),
                    ))),
                Center(
                    child: ButtonBar(
                  buttonPadding: EdgeInsets.all(5),
                  mainAxisSize: MainAxisSize
                      .min, // this will take space as minimum as posible(to center)
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50),
                        child: new FlatButton(
                            color: Colors.lightBlue.shade600,
                            child: new Icon(
                              Icons.clear,
                              size: 64.0,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              String commentText =
                                  commentTextFieldController.text;
                              updateDetectionFeedback(
                                  detectionID, 0, commentText);
                              widget.callbackFunction();
                              Navigator.pop(context);
                            },
                            shape: new RoundedRectangleBorder(
                                borderRadius:
                                    new BorderRadius.circular(30.0)))),
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50),
                        child: new FlatButton(
                            color: Colors.lightBlue.shade600,
                            child: new Icon(
                              Icons.check,
                              size: 64.0,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              String commentText =
                                  commentTextFieldController.text;
                              updateDetectionFeedback(
                                  detectionID, 1, commentText);
                              widget.callbackFunction();
                              Navigator.pop(context);
                            },
                            shape: new RoundedRectangleBorder(
                                borderRadius:
                                    new BorderRadius.circular(30.0)))),
                  ],
                )),
                Padding(
                    padding: const EdgeInsets.all(20),
                    child: TextField(
                      controller: commentTextFieldController,
                      maxLines: 3,
                      maxLength: 255,
                      decoration: InputDecoration(
                        hintText: "Comment (optional)",
                        contentPadding: EdgeInsets.all(5),
                        border: const OutlineInputBorder(),
                      ),
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
